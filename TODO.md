# ToDo

- Add an abstract base class for the checks
- Add some tests
- Catch parser error gracefully
- Offer option to choose between HermiT and Pellet
- Copy features from the OntoCheck plugin of Protégé
- Create a Gitlab pipline that automatically pushed to PyPI when a tag is created
- Relax version requirements for dependencies and add testing with different Python versions
- Git using OOPS to check files that are not ontologies (e.g. shape files)
